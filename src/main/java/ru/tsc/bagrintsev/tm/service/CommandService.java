package ru.tsc.bagrintsev.tm.service;

import ru.tsc.bagrintsev.tm.api.ICommandRepository;
import ru.tsc.bagrintsev.tm.api.ICommandService;
import ru.tsc.bagrintsev.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;


    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getAvailableCommands() {
        return commandRepository.getAvailableCommands();
    }
}
