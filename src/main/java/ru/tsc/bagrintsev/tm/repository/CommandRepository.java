package ru.tsc.bagrintsev.tm.repository;

import ru.tsc.bagrintsev.tm.api.ICommandRepository;
import ru.tsc.bagrintsev.tm.constant.CommandLineConst;
import ru.tsc.bagrintsev.tm.constant.InteractionConst;
import ru.tsc.bagrintsev.tm.model.Command;

import static ru.tsc.bagrintsev.tm.constant.CommonConst.EMPTY;

public final class CommandRepository implements ICommandRepository {

    public static final Command HELP = new Command(CommandLineConst.HELP_SHORT, InteractionConst.HELP, "Print help.");

    public static final Command ABOUT = new Command(CommandLineConst.ABOUT_SHORT, InteractionConst.ABOUT, "Print about.");

    public static final Command VERSION = new Command(CommandLineConst.VERSION_SHORT, InteractionConst.VERSION, "Print version.");

    public static final Command INFO = new Command(CommandLineConst.INFO_SHORT, InteractionConst.INFO, "Print system info.");

    public static final Command EXIT = new Command(EMPTY, InteractionConst.EXIT, "Close program.");

    public static final Command ARGUMENTS = new Command(CommandLineConst.ARGUMENTS_SHORT, InteractionConst.ARGUMENTS, "Print command-line arguments.");

    public static final Command COMMANDS = new Command(CommandLineConst.COMMANDS_SHORT, InteractionConst.COMMANDS, "Print application interaction commands.");


    private final Command[] commands = new Command[]{
            HELP,
            ABOUT,
            VERSION,
            INFO,
            ARGUMENTS,
            COMMANDS,
            EXIT
    };

    @Override
    public Command[] getAvailableCommands() {
        return commands;
    }
}
