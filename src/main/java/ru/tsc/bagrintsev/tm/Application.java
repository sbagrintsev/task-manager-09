package ru.tsc.bagrintsev.tm;

import ru.tsc.bagrintsev.tm.api.ICommandService;
import ru.tsc.bagrintsev.tm.component.Bootstrap;
import ru.tsc.bagrintsev.tm.model.Command;
import ru.tsc.bagrintsev.tm.repository.CommandRepository;
import ru.tsc.bagrintsev.tm.api.ICommandRepository;
import ru.tsc.bagrintsev.tm.service.CommandService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.tsc.bagrintsev.tm.constant.CommandLineConst.*;
import static ru.tsc.bagrintsev.tm.constant.InteractionConst.*;
import static ru.tsc.bagrintsev.tm.util.FormatUtil.*;

/**
 * @author Sergey Bagrintsev
 * @version 1.9.0
 */

public final class Application {

    public static void main(final String[] args) throws IOException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
