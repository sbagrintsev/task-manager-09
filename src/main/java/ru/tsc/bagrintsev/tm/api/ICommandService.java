package ru.tsc.bagrintsev.tm.api;

import ru.tsc.bagrintsev.tm.model.Command;

public interface ICommandService {

    Command[] getAvailableCommands();

}
