package ru.tsc.bagrintsev.tm.api;

public interface ICommandController {
    void showSystemInfo();

    void showWelcome();

    void showHelp();

    void showCommands();

    void showArguments();

    void showVersion();

    void showAbout();

    void showError(String arg);

    void showOnStartError(String arg);
}
