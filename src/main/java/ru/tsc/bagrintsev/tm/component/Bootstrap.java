package ru.tsc.bagrintsev.tm.component;

import ru.tsc.bagrintsev.tm.api.ICommandController;
import ru.tsc.bagrintsev.tm.api.ICommandRepository;
import ru.tsc.bagrintsev.tm.api.ICommandService;
import ru.tsc.bagrintsev.tm.controller.CommandController;
import ru.tsc.bagrintsev.tm.repository.CommandRepository;
import ru.tsc.bagrintsev.tm.service.CommandService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import static ru.tsc.bagrintsev.tm.constant.CommandLineConst.*;
import static ru.tsc.bagrintsev.tm.constant.InteractionConst.*;

public class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    public void run(final String[] args) throws IOException {
        processOnStart(args);

        commandController.showWelcome();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(System.in))) {
            while (true) {
                System.out.println();
                System.out.println("Enter Command:");
                System.out.print(">> ");
                final String command = reader.readLine();
                processOnTheGo(command);
            }
        }
    }

    private void processOnStart(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case HELP_SHORT:
                commandController.showHelp();
                break;
            case VERSION_SHORT:
                commandController.showVersion();
                break;
            case ABOUT_SHORT:
                commandController.showAbout();
                break;
            case INFO_SHORT:
                commandController.showSystemInfo();
                break;
            case ARGUMENTS_SHORT:
                commandController.showArguments();
                break;
            case COMMANDS_SHORT:
                commandController.showCommands();
                break;
            default:
                commandController.showOnStartError(arg);
                commandController.showHelp();
        }
    }

    private void processOnTheGo(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case HELP:
                commandController.showHelp();
                break;
            case VERSION:
                commandController.showVersion();
                break;
            case ABOUT:
                commandController.showAbout();
                break;
            case INFO:
                commandController.showSystemInfo();
                break;
            case EXIT:
                close();
                break;
            case ARGUMENTS:
                commandController.showArguments();
                break;
            case COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showError(command);
                commandController.showHelp();
        }
    }

    private void processOnStart(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        processOnStart(arg);
        close();
    }

    private void close() {
        System.exit(0);
    }

}
